Source: libgrss
Section: libs
Priority: extra
Maintainer: Jon Bernard <jbernard@debian.org>
Build-Depends: debhelper (>= 9),
               dh-autoreconf,
               gobject-introspection,
               libxml2-dev (>= 2.9.2),
               gtk-doc-tools (>= 1.21),
               libsoup2.4-dev (>= 2.48.0),
               libglib2.0-dev (>= 2.42.1),
               libgirepository1.0-dev (>= 1.42)
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/collab-maint/libgrss.git
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/libgrss.git

Package: libgrss0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Glib-based library to manage RSS and Atom feeds
 libgrss is a Glib abstraction library for handling feeds in RSS, Atom and
 other formats. It is intended to be used to manage syndication of feeds in a
 convenient way.

Package: libgrss-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libgrss0 (= ${binary:Version}), libxml2-dev, libsoup2.4-dev
Description: Glib-based library to manage RSS and Atom feeds - development files
 libgrss is a Glib abstraction library for handling feeds in RSS, Atom and
 other formats. It is intended to be used to manage syndication of feeds in a
 convenient way.
 .
 Install this package if you wish to develop your own programs using the
 libgrss library.

Package: gir1.2-grss-0.7
Section: introspection
Architecture: any
Depends: ${gir:Depends}, ${misc:Depends}
Description: GObject introspection data for libgrss
 libgrss is a Glib abstraction library for handling feeds in RSS, Atom and
 other formats. It is intended to be used to manage syndication of feeds in a
 convenient way.
 .
 This package contains introspection data for libgrss.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.
